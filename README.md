Cux Social Post
===============
Post to Facebook, GooglePlus, LinkedIn

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist cux-social-post/yii2-social-post "*"
```

or add

```
"cux-social-post/yii2-social-post": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \cux\social\AutoloadExample::widget(); ?>```